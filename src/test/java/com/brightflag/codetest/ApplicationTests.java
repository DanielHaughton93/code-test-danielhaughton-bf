package com.brightflag.codetest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.brightflag.domain.SubjectSignUp;
import com.brightflag.service.SubjectSignUpService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	//NOTE
	//HAVENT WORKED WITH FRONT-END SPRING MVC MUCH
	
	@Autowired 
	SubjectSignUpService subjectSignUpService;
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Test
	public void testHappy() {
		int updatedRows = subjectSignUpService.signStudentUpForSubject(1, 2); //mary,math
		System.out.println("Number of updated rows:" + updatedRows);
		assertEquals(true,updatedRows> 0);
		//lets retrieve all the rows
		// it should have mary being signed up for math
		//better option is  to use something more like " a getSubjectSignUps where studentId = x here"
		List<SubjectSignUp> allRows = subjectSignUpService.getSubjectSignUps();
		boolean insertedCorrectly = false;
		for(int i = 0;i< allRows.size(); i++) {
			if(allRows.get(i).getStudentId() == 1 && allRows.get(i).getSubjectId() ==2) {
				insertedCorrectly = true;
				break;
			}
		}
		assertEquals(true, insertedCorrectly);
	}
	
	@Test
	public void testSad() {
		//lets add foreign key references that primary keys that dont exist
		//should not insert
		int updatedRows = subjectSignUpService.signStudentUpForSubject(1000, 20000);
		System.out.println("Number of updated rows:" + updatedRows);
		assertEquals(true,updatedRows == 0);
	}
	
	

}
