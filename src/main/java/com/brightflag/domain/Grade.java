package com.brightflag.domain;

public class Grade {
	private Integer gradeId;
	private Integer examID;
	//probably no need to have examName here, since we have Id of exam?
	private String examName;
	private Integer studentId;
	private double gradePercentage;
	
	

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public Integer getExamID() {
		return examID;
	}

	public void setExamID(Integer examID) {
		this.examID = examID;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public double getGradePercentage() {
		return gradePercentage;
	}

	public void setGradePercentage(double gradePercentage) {
		this.gradePercentage = gradePercentage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examID == null) ? 0 : examID.hashCode());
		result = prime * result + ((examName == null) ? 0 : examName.hashCode());
		result = prime * result + ((gradeId == null) ? 0 : gradeId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(gradePercentage);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((studentId == null) ? 0 : studentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grade other = (Grade) obj;
		if (examID == null) {
			if (other.examID != null)
				return false;
		} else if (!examID.equals(other.examID))
			return false;
		if (examName == null) {
			if (other.examName != null)
				return false;
		} else if (!examName.equals(other.examName))
			return false;
		if (gradeId == null) {
			if (other.gradeId != null)
				return false;
		} else if (!gradeId.equals(other.gradeId))
			return false;
		if (Double.doubleToLongBits(gradePercentage) != Double.doubleToLongBits(other.gradePercentage))
			return false;
		if (studentId == null) {
			if (other.studentId != null)
				return false;
		} else if (!studentId.equals(other.studentId))
			return false;
		return true;
	}

	

	

}

