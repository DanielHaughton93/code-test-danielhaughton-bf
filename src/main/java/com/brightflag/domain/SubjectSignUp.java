package com.brightflag.domain;

public class SubjectSignUp {
	private Integer subjectSignUpId;
	private Integer subjectId;
	private Integer studentId;
	public Integer getSubjectSignUpId() {
		return subjectSignUpId;
	}
	public void setSubjectSignUpId(Integer subjectSignUpId) {
		this.subjectSignUpId = subjectSignUpId;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((studentId == null) ? 0 : studentId.hashCode());
		result = prime * result + ((subjectId == null) ? 0 : subjectId.hashCode());
		result = prime * result + ((subjectSignUpId == null) ? 0 : subjectSignUpId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectSignUp other = (SubjectSignUp) obj;
		if (studentId == null) {
			if (other.studentId != null)
				return false;
		} else if (!studentId.equals(other.studentId))
			return false;
		if (subjectId == null) {
			if (other.subjectId != null)
				return false;
		} else if (!subjectId.equals(other.subjectId))
			return false;
		if (subjectSignUpId == null) {
			if (other.subjectSignUpId != null)
				return false;
		} else if (!subjectSignUpId.equals(other.subjectSignUpId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SubjectSignUp [subjectSignUpId=" + subjectSignUpId + ", subjectId=" + subjectId + ", studentId="
				+ studentId + "]";
	}
	
	
}