package com.brightflag.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brightflag.domain.Student;
import com.brightflag.domain.SubjectSignUp;

@Repository
public class SubjectSignUpRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public int signStudentUpForSubject(Integer studentId, Integer subjectId) {
		try {
		int result =  jdbcTemplate.update(
			    "INSERT INTO subjectSignUp (studentId, subjectId) VALUES (?, ?)",
			    studentId, subjectId
			);
		return result;
		}
		catch (Exception e) {
			// something went wrong
			//return 0 as 0 rows were updated
			// this should really be expanded
			//multiple exceptions could be handled more specifically
			return 0;
		}
		
	}
	
	public List<SubjectSignUp> getSubjectSignUps() {
		return jdbcTemplate.query("SELECT subjectSignUpId, subjectId, studentId FROM subjectSignUp;",
				new BeanPropertyRowMapper<SubjectSignUp>(SubjectSignUp.class));
	}
}
