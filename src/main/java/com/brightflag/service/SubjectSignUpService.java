package com.brightflag.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brightflag.domain.SubjectSignUp;
import com.brightflag.repository.SubjectSignUpRepository;

@Service
public class SubjectSignUpService {

	@Autowired
	SubjectSignUpRepository subjectSignUpRepository;
	//this method could also take in SubjectSignUp object
	public int signStudentUpForSubject(Integer studentId, Integer subjectId) {
		return subjectSignUpRepository.signStudentUpForSubject(studentId,subjectId);
	}
	public List<SubjectSignUp> getSubjectSignUps(){
		return subjectSignUpRepository.getSubjectSignUps();
	}
}
