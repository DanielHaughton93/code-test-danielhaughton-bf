drop table student if exists;
drop table subject if exists;
drop table faculty if exists;
drop table exam if exists;
drop table grade if exists;
drop table subjectSignUp if exists;
create table student (
   studentID integer auto_increment not null,
   firstName varchar(255) not null,
   lastName varchar(255) not null,
   primary key(studentID)
);

create table subject (
   subjectID integer auto_increment not null,
   subjectName varchar(255) not null,
   primary key(subjectID)
);

create table faculty (
   facultyID integer auto_increment not null,
   facultyName varchar(255) not null,
   primary key(facultyID)
);

create table exam (
   examID integer auto_increment not null,
   examName varchar(255) not null,
   primary key(examID)
);

create table grade (
   gradeId integer auto_increment not null,
   examId integer not null,
   examName varchar(255) not null,
   studentId integer not null,
   gradePercentage double not null,
   CONSTRAINT FK_grade_studentId FOREIGN KEY (studentId)
   REFERENCES student(studentId),
   CONSTRAINT FK_grade_examId FOREIGN KEY (examId)
   REFERENCES exam(examId),
   primary key(gradeId)
);

create table subjectSignUp (
   subjectSignUpId integer auto_increment not null,
   subjectId integer not null,
   studentId integer not null,
   CONSTRAINT FK_subjectSignUp_studentId FOREIGN KEY (studentId)
   REFERENCES student(studentId),
   CONSTRAINT FK_subjectSignUp_subjectId FOREIGN KEY (subjectId)
   REFERENCES subject(subjectId),
   primary key(subjectSignUpId)
);